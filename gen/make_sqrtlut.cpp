#include <stdlib.h>
#include <fstream>
#include <math.h>


int main( int argc, char** argv ) {

  std::ofstream myGeneratedCode( argv[1] );
  for( int i = 0; i <= 1000; i++ ) {
    myGeneratedCode << "sqrtLUT.push_back(" << sqrt((float) i / 100.0f) << ");\n";
  }
  return 0;
  
}