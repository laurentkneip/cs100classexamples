template<typename... Elements> class MyTuple;

template<typename Head, typename... Tail>
class MyTuple<Head, Tail...> : private MyTuple<Tail...> {
  Head head;
public:
  void* accessElement( int index ) {
    if( index==0 )
      return &head;
    return MyTuple<Tail...>::accessElement(index-1);
  };
};

template<> class MyTuple<> {
public:
  void* accessElement(int index) {
    return nullptr;
  }
};

int main() {
  MyTuple<int,double,double,int> testTuple;
  *((double*) testTuple.accessElement(1)) = 1.0;
}
