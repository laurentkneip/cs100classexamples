// C program to demonstrate use of variable number of arguments
#include <stdarg.h>
#include <stdio.h>


int mean(int arg_count, ...) {
  va_list ap;
  va_start(ap, arg_count);
  int sum = va_arg(ap, int);

  for (int i = 1; i < arg_count; i++)
    sum += va_arg(ap, int);

  va_end(ap);
    return sum / arg_count;
}

int main() {
  printf("The mean is %d", mean(5, 12, 67, 6, 7, 100));
  printf("The mean is %d", mean(6, 12, 67, 6, 7, 100,10000));
  printf("The mean is %d", mean(1, 12 ));
  return 0;
}