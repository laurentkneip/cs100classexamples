#include "CS100Config.h"
#include "MessagePrinter.hpp"
#include <iostream>

int main() {

#ifdef DEBUGMODE
  std::cout << "Welcome to CS100 examples version ";
  std::cout << CS100EXAMPLES_VERSION_MAJOR << "." << CS100EXAMPLES_VERSION_MINOR << "\n";
  std::cout << "This binary tests MessagePrinter\n";
#endif

  MessagePrinter mp( std::string("Hello World") );
  mp.print();

  return 0;
}