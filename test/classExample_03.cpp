#include <vector>
#include <thread>
#include <iostream>
#include <mutex>


class Counter {
public:
  Counter() {
    m_value = 0;
  };

  int getValue() {
    return m_value;
  };

  void increment() {
    m_mutex.lock();
    m_value++;
    m_mutex.unlock();
  };

private:
  int m_value;
  std::mutex m_mutex;
};

void incrementCounterManyTimes( Counter & counter ) {
  for( int i = 0; i < 5000; i++ ) {
    counter.increment();
  }
}

int main() {
  Counter counter;
  std::vector<std::thread> threads;

  for( int i = 0; i < 5; i++ ) {
    threads.push_back( std::thread(incrementCounterManyTimes, std::ref(counter) ) );
  }

  for( int i = 0; i < 5; i++ ) {
    threads[i].join();
  }

  std::cout << counter.getValue() << "\n";
  return 0;
}