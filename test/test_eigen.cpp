#include <stdlib.h>
#include <Eigen/Eigen>
#include <iostream>

int main() {
  
  //small test program for demonstrating some of the features of Eigen

  //Example 1: Finding the intersection point between two lines, which are defined by two points

  /*
  //Create four random points in the plane
  Eigen::Vector2d p1l1 = Eigen::Vector2d::Random();
  Eigen::Vector2d p2l1 = Eigen::Vector2d::Random();
  Eigen::Vector2d p1l2 = Eigen::Vector2d::Random();
  Eigen::Vector2d p2l2 = Eigen::Vector2d::Random();

  //Homogenize the vectors
  Eigen::Vector3d p1l1hom; p1l1hom << p1l1, 1.0f;
  Eigen::Vector3d p2l1hom; p2l1hom << p2l1, 1.0f;
  Eigen::Vector3d p1l2hom; p1l2hom << p1l2, 1.0f;
  Eigen::Vector3d p2l2hom; p2l2hom << p2l2, 1.0f;

  //Extract the line parameters
  Eigen::Vector3d l1 = p1l1hom.cross(p2l1hom);
  Eigen::Vector3d l2 = p1l2hom.cross(p2l2hom);

  //Extract the intersection point (in homogeneous form)
  Eigen::Matrix<double,2,3> A;
  A.row(0) = l1.transpose();
  A.row(1) = l2.transpose();

  Eigen::JacobiSVD< Eigen::Matrix<double,2,3> > svd(A, Eigen::ComputeFullV );
  Eigen::Vector3d intersectionPoint = svd.matrixV().col(2);
  std::cout << "The recovered intersection point is :" << intersectionPoint.transpose() << std::endl;

  //verification that this point lies on both lines
  std::cout << l1.dot(intersectionPoint) << std::endl;
  std::cout << l2.transpose() * intersectionPoint << std::endl;
  */

  //Example 2: Apply a blur kernel to an image
  Eigen::Array<float,20,20> image;
  image.fill(0);
  image(4,8) = 1.0f;
  image(12,3) = 1.0f;
  image(17,15) = 1.0f;

  std::cout << "The image before the kernel application:\n" <<  image << "\n\n" << std::endl;
  
  //generate the kernel
  Eigen::Array<float,5,5> K;
  K << 0.0f, 1.0f, 2.0f, 1.0f, 0.0f,
       1.0f, 3.0f, 4.0f, 3.0f, 1.0f,
       2.0f, 4.0f, 9.0f, 4.0f, 2.0f,
       1.0f, 3.0f, 4.0f, 3.0f, 1.0f,
       0.0f, 1.0f, 2.0f, 1.0f, 0.0f;
  float Knorm = sqrt( (K*K).sum() );
  K /= Knorm;
  
  //generate the convolved image
  Eigen::Array<float,20,20> imageConv;
  imageConv.fill(0);

  for( int r = 2; r < (20-2); r++ ) {
    for( int c = 2; c < (20-2); c++ ) {
      imageConv(r,c) = (image.block(r-2,c-2,5,5) * K).sum();
    }
  }

  std::cout <<"The image after the kernel application:\n" << imageConv << "\n\n" << std::endl;

  //generate another convolved image
  Eigen::Array<float,20,20> imageConv2;
  imageConv2.fill(0);

  for( int r = 2; r < (20-2); r++ ) {
    for( int c = 2; c < (20-2); c++ ) {
      imageConv2(r,c) = (imageConv.block(r-2,c-2,5,5) * K).sum();
    }
  }

  std::cout <<"The image after another kernel application:\n" << imageConv2 << "\n\n" << std::endl;
  
  return 0;    
}