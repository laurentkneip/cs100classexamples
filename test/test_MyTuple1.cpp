template<typename... Elements> class MyTuple;

template<typename Head, typename... Tail>
class MyTuple<Head, Tail...> : private MyTuple<Tail...> {
  Head head;
public:
  void* accessElement( int i ) {
    if( i == 0 )
      return &head;
    return MyTuple<Tail...>::accessElement(i-1);
  };
};

template<> class MyTuple<> {
public:
  void* accessElement( int i ) {
    return nullptr;
  };
};

int main() {
  MyTuple<int,double,double,int> testTuple;

  *((int *) testTuple.accessElement(0)) = 1;
  *((double *) testTuple.accessElement(1)) = 2;
}
