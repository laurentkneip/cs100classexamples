#include <iostream>
#include <thread>
#include <mutex>
 
std::once_flag flag1;

void printHello() {
  std::cout << "Hello\n";
}

void threadFunction()
{
  std::call_once(flag1, printHello);
}
 
int main()
{
    std::thread st1(threadFunction);
    std::thread st2(threadFunction);
    std::thread st3(threadFunction);
    std::thread st4(threadFunction);
    st1.join();
    st2.join();
    st3.join();
    st4.join();

    return 0;
}