#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <thread>

class OutOfBoundsExc : public std::exception {
public:
  OutOfBoundsExc( const std::string & containerName, int index, int size ) :
    m_containerName(containerName), m_index(index), m_size(size) {}
  void print() {
    std::cout << "Attempt to access element " << m_index
              << " in " << m_containerName << " of size " << m_size << "\n";
  }
private:
  std::string m_containerName;
  int m_index;
  int m_size;
};

class MyContainer {
public:
  void resizeContainer( int size ) {
    m_data.resize(size);
  };

  double & accessElement( int index ) {
    if( index >= m_data.size() )
      throw OutOfBoundsExc(std::string("MyContainer"),index,m_data.size());
    return m_data[index];
  }

private:
  std::vector<double> m_data;
};

int main() {
  MyContainer container;
  int containerSize = 10;
  container.resizeContainer(containerSize);

  int index = 0;

  while(1) {
    try {
      container.accessElement(index) = (double) index;
      std::cout << "Finished iteration " << index++ << std::endl;
    } catch( OutOfBoundsExc & e ) {
      e.print();
      containerSize += 10;
      container.resizeContainer(containerSize);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(500));
  }
}