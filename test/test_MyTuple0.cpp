template<typename... Elements> class MyTuple;

template<typename Head, typename... Tail>
class MyTuple<Head, Tail...> : private MyTuple<Tail...> {
  Head head;
public:
  Head* accessElement( int i ) {
    if( i == 0 )
      return &head;
    return MyTuple<Tail...>::accessElement(i-1);
  };
};

template<> class MyTuple<> {
public:
  int* accessElement( int i ) {
    return nullptr;
  };
};

int main() {
  MyTuple<int,int,int,int> testTuple;

  *(testTuple.accessElement(0)) = 1;
  *(testTuple.accessElement(1)) = 2;
}
