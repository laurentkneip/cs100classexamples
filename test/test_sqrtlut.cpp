#include <stdlib.h>
#include <vector>
#include <iostream>
#include <math.h>

int main() {

  //only do once

  std::vector<float> sqrtLUT;
  
#include "sqrtlut.inc"
  
  //can do many times

  for( int i = 0; i < 10; i++ ) {
    float sample = ((float) rand() / (float) RAND_MAX) * 10.0f;
    
    int index = floor(sample * 100.0f + 0.5f);
    if (index < 0) index = 0;
    if (index > 1000) index = 1000;

    std::cout << "The square-root of " << sample << " is almost " << sqrtLUT[index] << ".";
    std::cout << " The absolute error by the look-up table would is " << fabs(sqrtLUT[index] - sqrt(sample)) << "\n";
  }
}