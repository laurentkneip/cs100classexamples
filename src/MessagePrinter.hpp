#ifndef MESSAGEPRINTER_HPP_
#define MESSAGEPRINTER_HPP_

#include <stdlib.h>
#include <string>
#include <iostream>

class MessagePrinter {
public:
  MessagePrinter( const std::string & message );

  void print();

private:
  std::string m_message;
};

#endif