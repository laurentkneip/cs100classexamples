#include "MessagePrinter.hpp"

MessagePrinter::MessagePrinter( const std::string & message ) : m_message(message) {}

void
MessagePrinter::print() { std::cout << m_message << std::endl; }